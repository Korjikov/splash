# Splash

> Built with the [Phoenix Framework](https://github.com/phoenixframework/phoenix)

To start your new Phoenix application you have to:

1. Clone this repo, then cd to the new directory
1. run with `docker-compose up --build -d`
2. Install dependencies with `mix deps.get`
3. (optional) Install npm dependencies to customize the ES6 js/Sass `npm install`
4. Start Phoenix router with `mix phx.server`

Now you can visit `localhost:4000` from your browser.


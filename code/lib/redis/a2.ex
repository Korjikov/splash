defmodule Redis.A2 do
  use GenServer

  ### GenServer API

  @doc """
  Функция обратного вызова для GenServer.init/1
  """

  defp query_data(count, srcnum) when is_number(count) do
    query_data(count, [], srcnum)
  end
  
  defp query_data(count, query_list, srcnum) when count >= 1 do
    
    #start_data = DateTime.to_unix(DateTime.utc_now()) #+ Enum.random(0..1000)
    #query =    "{\"src\":\"" <> to_string(srcnum) <> 
    #          "\",\"dst\":\"" <> to_string(Enum.random(77274560000..77274569999)) <> 
    #          "\",\"times\":\"" <> to_string( start_data ) <> 
    #          "\",\"timee\":\"" <> to_string( start_data + 100 ) <> 
    #          "\"}"
    
    start_data = DateTime.to_unix(DateTime.utc_now()) + Enum.random(0..1000)
    query =    "{\"src\":\"" <> to_string(Enum.random(77274700000..77274799999)) <> 
              "\",\"dst\":\"" <> to_string(Enum.random(77274560000..77274569999)) <> 
              "\",\"times\":\"" <> to_string( start_data ) <> 
              "\",\"timee\":\"" <> to_string( start_data + Enum.random(0..10) ) <> 
              "\"}"
    
    srcnum = if srcnum == 79999999999 do 70000000000 else srcnum + 1 end
    #IO.puts "Query: " <> query
    query_list = query_list ++ [["RPUSH", "cdrlist", query]]
    query_data(count-1, query_list, srcnum)
  end
  
  defp query_data(count, query_list, srcnum) do
    query_list
  end
  
  def init(state) do
    {:ok, conn} = Redix.start_link(host: "splash_redis_1", port: 6379)
    IO.puts "Init Redis.A2"
    #IO.inspect({self()})
    #GenServer.cast(self(), {:enqueue, "23876487234"})
    IO.puts DateTime.utc_now()
    cycle(50000,conn)
    IO.puts DateTime.utc_now()
    {:ok, state}
  end
  
  def start() do
    {:ok, conn} = Redix.start_link(host: "splash_redis_1", port: 6379)
    IO.puts "Start Redis.A2"
    #IO.inspect({self()})
    #GenServer.cast(self(), {:enqueue, "23876487234"})
    IO.puts DateTime.utc_now()
    cycle(50000,conn)
    IO.puts DateTime.utc_now()
    #function diff
    :ok
  end
  
  defp cycle( count, conn ) when is_number(count) do
    cycle( 0, count, conn, DateTime.truncate(DateTime.utc_now(), :second ), 0 , 70000000000)
  end
  
  defp cycle( i, count, conn, start_time, per_time_cout, srcnum ) when count >= 1 do
    pipeline_count = 10
    Redix.pipeline( conn, query_data(pipeline_count, srcnum) )
    cur_time = DateTime.truncate( DateTime.utc_now(), :second )
    { per_time_cout, start_time } = 
      if ( DateTime.compare(start_time, cur_time) == :lt ) do 
        #IO.puts "records: " <> to_string( per_time_cout * pipeline_count ) <> ", DateTime: " <> to_string( start_time )
        { 0, DateTime.truncate( DateTime.utc_now(), :second ) }  
      else 
        { per_time_cout, start_time } 
      end
    srcnum = srcnum + pipeline_count
    cycle( i + 1, count - 1, conn, start_time, per_time_cout + 1, srcnum )
  end

  defp cycle( total, 0, conn, datetime, per_time_cout, srcnum ) do
    IO.puts "#{total} counts"
    :ok
  end
  
  @doc """
  Функции обратного вызова для GenServer.handle_call/3
  """
  def handle_call(:dequeue, _from, [value | state]) do
    {:reply, value, state}
  end

  def handle_call(:dequeue, _from, []), do: {:reply, nil, []}

  def handle_call(:queue, _from, state), do: {:reply, state, state}

  @doc """
  Функция обратного вызова для GenServer.handle_cast/2
  """
  def handle_cast({:enqueue, value}, state) do
    IO.puts "hello #{value}"
    {:noreply, state ++ [value]}
  end

  ### Клиентский API

  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def queue, do: GenServer.call(__MODULE__, :queue)
  def enqueue(value), do: GenServer.cast(__MODULE__, {:enqueue, value})
  def dequeue, do: GenServer.call(__MODULE__, :dequeue)
end
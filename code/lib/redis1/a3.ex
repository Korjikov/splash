defmodule Redis1.A3 do

  def cycle() do
    {:ok, conn} = Redix.start_link(host: "splash_redis_1", port: 6379)
    cycle(conn,DateTime.truncate(DateTime.utc_now(), :second ),0)
  end
  
  def cycle(conn,start_time, per_time_cout) do
    #{:ok, data } = Redix.command(conn, ["lpop", "cdrlist"])
    {:ok, count} = Redix.command(conn, ["evalsha", "0881605a183e1199a88ef31978823a8a5c744518", "0"])
    if count != 0 do 
      [len | count1] = count
      if len > 1 do
        #IO.inspect count
        #IO.puts length(count)
        [src, dst, times] = count1
        #[dst | count1 ] = count1
        #[times | count1 ] = count1
        {:ok,_} = Redix.command(conn, ["rpush", "list:alert", to_string(len)<>":"<>to_string(src)<>":"<>to_string(dst)<>":"<>to_string(times)])
        #IO.inspect count
      end
    end
    
    cur_time = DateTime.truncate( DateTime.utc_now(), :second )
    #if ( DateTime.compare(start_time, cur_time) == :lt ) do
    #  IO.puts "readers: " <> to_string( per_time_cout) <> ", DateTime: " <> to_string( start_time )
    #end
    per_time_cout = if ( DateTime.compare(start_time, cur_time) == :lt ) do 0 else per_time_cout end
    start_time = if ( DateTime.compare(start_time, cur_time) == :lt ) do DateTime.truncate( DateTime.utc_now(), :second ) else start_time end

    cycle(conn, start_time, per_time_cout + 1)
  end

end
defmodule Chat do
  use Application
  
  defp cycle( ) do
    IO.puts "counts"
    :ok
  end
  
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    
    task1 = Task.async(Redis1.A3, :cycle, [])
    task2 = Task.async(Redis.A2, :start, [])

    #IO.inspect(task)
    
    #{:ok, _} = GenServer.start_link(Redis.A2, [:hello])
    #{:ok, _} = GenServer.start_link(Redis1.A3, [:hello])
    
    children = [
      supervisor(Chat.Endpoint, [],name: Chat.Supervisor),
      #supervisor(Task.Supervisor, [[name: Exampleahat.Supervisor, restart: :transient]]),
      #worker(Redis.A2, [],id: 1)
      worker(Task, [Chat, :listen, [6060]])
      #worker(Redis1.A3,[],id: 2)
    ]
    #opts = [strategy: :one_for_one, name: Chat.Supervisor]
    {:ok, pid} = Supervisor.start_link(children, strategy: :one_for_one)
    #%{active: a, specs: b, supervisors: c, workers: d} = Supervisor.count_children(pid)
    #IO.puts to_string(a) <> to_string(b) <> to_string(c) <> to_string(d)
    #IO.puts "Application Chat.Endpoint"
    #IO.inspect(pid)
    
    #{:ok, pid1} = Task.Supervisor.start_child(Exampleahat.Supervisor, fn -> IO.puts "sample function !!!!!!!!!!!!!!!!" end)
    
    {:ok, pid}
  end
  def config_change(changed, _new, removed) do
    Chat.Endpoint.config_change(changed, removed)
    :ok
  end
  
  defp analyze( data ) when length(data) == 0 do [] end
  defp analyze( data ) when length(data) > 0 do analyze( data, [], 0, 0 ) end
  defp analyze( data, data1, billstart, count ) when length(data) == 0 do data1 end
  defp analyze( data, data1, billstart, count ) when length(data) > 0 do
    [line | data] = data
    [ {cseq_num, cseq_type}, {reason, reason_code}, date ] = line
    
    billing = if (count > 0) do
      case cseq_type do 
        :invite ->
          case reason do
            "Trying" -> if billstart == 1 do 2 else -1 end
            "Session Progress" -> if billstart == 2 do 3 else -1 end
            "Ringing" -> if billstart == 2 do 3 else -1 end
            "OK" -> if billstart == 3 do 4 else -1 end
            "Unauthorized" -> -1
            "Request Terminated" -> -1
            "Service Unavailable" -> -1
            _ -> -1
          end
        :ack ->
          case reason do
            "ack" -> if billstart == 4 do 5 else -1 end
            _ -> -1
          end
        :options ->
          case reason do
            "Not Found" -> billstart
            "options" -> billstart
            "OK" -> billstart
          end
        :bye ->
          case reason do
            "bye" -> if billstart == 5 do 6 else -1 end
            "OK" -> if billstart == 6 do 7 else -1 end
          end
        :register ->
          case reason do
            "Unauthorized" -> -1
            "register" -> -1
            "OK" -> -1
          end
        :calcel ->
          case reason do
            "cancel" -> -1
            "OK" -> -1
          end
        _ -> -1
      end
    else
      -1
    end
    
    billing = if (count == 0 and cseq_type == :invite and reason == "invite") do 1 else billing end #Первый invite
    
    data1 = data1 ++ [[ { cseq_num, cseq_type}, {reason, reason_code} , date, billing]]
    analyze( data, data1, billing, count + 1 )
    
  end
  defp global_spr() do
    spr = %{
      "1" => {"США", "USA"},
      "1" => {"Канада", "CAN"},
      "1242" => {"Багамские Острова", "BHS"},
      "1246" => {"Барбадос", "BRB"},
      "1264" => {"Ангилья", "AIA"},
      "1268" => {"Антигуа и Барбуда", "ATG"},
      "1284" => {"Британские Виргинские острова", "VGB"},
      "1340" => {"Американские Виргинские острова", "VIR"},
      "1345" => {"Каймановы острова", "CYM"},
      "1441" => {"Бермудские острова", "BMU"},
      "1473" => {"Гренада", "GRD"},
      "1649" => {"Тёркс и Кайкос", "TCA"},
      "1664" => {"Монтсеррат", "MSR"},
      "1670" => {"Северные Марианские острова Сайпан", "MNP"},
      "1671" => {"Гуам", "GUM"},
      "1684" => {"Американское Самоа", "ASM"},
      "1721" => {"Синт-Маартен", "SXM"},
      "1758" => {"Сент-Люсия", "LCA"},
      "1767" => {"Доминика", "DMA"},
      "1784" => {"Сент-Винсент и Гренадины", "VCT"},
      "1787" => {"Пуэрто-Рико", "PRI"},
      "1809" => {"Доминиканская Республика", "DOM"},
      "1829" => {"Доминиканская Республика", "DOM"},
      "1849" => {"Доминиканская Республика", "DOM"},
      "1868" => {"Тринидад и Тобаго", "TTO"},
      "1869" => {"Сент-Китс и Невис", "KNA"},
      "1876" => {"Ямайка", "JAM"},
      "1939" => {"Пуэрто-Рико", "PRI"},
      "20" => {"Египет", "EGY"},
      "211" => {"Южный Судан", "SSD"},
      "212" => {"Марокко", "MAR"},
      "212" => {"Западная Сахара", "ESH"},
      "213" => {"Алжир", "DZA"},
      "216" => {"Тунис", "TUN"},
      "218" => {"Ливия", "LBY"},
      "220" => {"Гамбия", "GMB"},
      "221" => {"Сенегал", "SEN"},
      "222" => {"Мавритания", "MRT"},
      "223" => {"Мали", "MLI"},
      "224" => {"Гвинея", "GIN"},
      "225" => {"Кот-д’Ивуар", "CIV"},
      "226" => {"Буркина Фасо", "BFA"},
      "227" => {"Нигер", "NER"},
      "228" => {"Того", "TGO"},
      "229" => {"Бенин", "BEN"},
      "230" => {"Маврикий", "MUS"},
      "231" => {"Либерия", "LBR"},
      "232" => {"Сьерра-Леоне", "SLE"},
      "233" => {"Гана", "GHA"},
      "234" => {"Нигерия", "NGA"},
      "235" => {"Чад", "TCD"},
      "236" => {"Центральноафриканская Республика", "CAF"},
      "237" => {"Камерун", "CMR"},
      "238" => {"Кабо-Верде", "CPV"},
      "239" => {"Сан-Томе и Принсипи", "STP"},
      "240" => {"Экваториальная Гвинея", "GNQ"},
      "241" => {"Габон", "GAB"},
      "242" => {"Конго", "COG"},
      "243" => {"Дем. Респ. Конго (Киншаса)", "COD"},
      "244" => {"Ангола", "AGO"},
      "244" => {"Кабинда", "AGO"},
      "245" => {"Гвинея-Бисау", "GNB"},
      "246" => {"Диего-Гарсия", "IOT"},
      "247" => {"Остров Вознесения", ""},
      "248" => {"Сейшелы", "SYC"},
      "249" => {"Судан", "SDN"},
      "250" => {"Руанда", "RWA"},
      "251" => {"Эфиопия", "ETH"},
      "252" => {"Сомали", "SOM"},
      "252" => {"Сомалиленд", ""},
      "252" => {"Пунтленд", ""},
      "253" => {"Джибути", "DJI"},
      "254" => {"Кения", "KEN"},
      "255" => {"Танзания", "TZA"},
      "256" => {"Уганда", "UGA"},
      "257" => {"Бурунди", "BDI"},
      "258" => {"Мозамбик", "MOZ"},
      "260" => {"Замбия", "ZMB"},
      "261" => {"Мадагаскар", "MDG"},
      "262" => {"Реюньон", "REU"},
      "262" => {"Майотта", "FRA"},
      "263" => {"Зимбабве", "ZWE"},
      "264" => {"Намибия", "NAM"},
      "265" => {"Малави", "MWI"},
      "266" => {"Лесото", "LSO"},
      "267" => {"Ботсвана", "BWA"},
      "268" => {"Свазиленд", "SWZ"},
      "269" => {"Коморы", "COM"},
      "27" => {"Южно-Африканская Респ.", "ZAF"},
      "290" => {"Остров Святой Елены", ""},
      "290" => {"Тристан-да-Кунья", ""},
      "291" => {"Эритрея", "ERI"},
      "297" => {"Аруба", "ABW"},
      "298" => {"Фарерские острова", "FRO"},
      "299" => {"Гренландия", "GRL"},
      "30" => {"Греция", "GRC"},
      "31" => {"Нидерланды", "NLD"},
      "32" => {"Бельгия", "BEL"},
      "33" => {"Франция", "FRA"},
      "34" => {"Испания", "ESP"},
      "34" => {"Каталония", "ESP"},
      "350" => {"Гибралтар", "GIB"},
      "351" => {"Португалия", "PRT"},
      "352" => {"Люксембург", "LUX"},
      "353" => {"Ирландия", "IRL"},
      "354" => {"Исландия", "ISL"},
      "355" => {"Албания", "ALB"},
      "356" => {"Мальта", "MLT"},
      "357" => {"Кипр", "CYP"},
      "358" => {"Финляндия", "FIN"},
      "35818" => {"Аланды", "ALA"},
      "359" => {"Болгария", "BGR"},
      "36" => {"Венгрия", "HUN"},
      "370" => {"Литва", "LTU"},
      "371" => {"Латвия", "LVA"},
      "372" => {"Эстония", "EST"},
      "373" => {"Молдова", "MDA"},
      "373" => {"Приднестровье", ""},
      "374" => {"Армения", "ARM"},
      "37447" => {"Нагорный Карабах", "AZE"},
      "375" => {"Белоруссия", "BLR"},
      "376" => {"Андорра", "AND"},
      "377" => {"Монако", "MCO"},
      "378" => {"Сан-Марино", "SMR"},
      "380" => {"Украина", "UKR"},
      "381" => {"Сербия", "SRB"},
      "381" => {"Косово", "UNK"},
      "381" => {"Воеводина", ""},
      "382" => {"Черногория", "MNE"},
      "385" => {"Хорватия", "HRV"},
      "386" => {"Словения", "SVN"},
      "387" => {"Босния и Герцеговина", "BIH"},
      "389" => {"Респ. Македония", "MKD"},
      "39" => {"Италия", "ITA"},
      "3906698" => {"Ватикан", "VAT"},
      "40" => {"Румыния", "ROU"},
      "41" => {"Швейцария", "CHE"},
      "420" => {"Чехия", "CZE"},
      "421" => {"Словакия", "SVK"},
      "423" => {"Лихтенштейн", "LIE"},
      "43" => {"Австрия", "AUT"},
      "44" => {"Великобритания", "GBR"},
      "44" => {"Англия", "GBR"},
      "44" => {"Шотландия", "GBR"},
      "44" => {"Уэльс", "GBR"},
      "441481" => {"Гернси", "GGY"},
      "441534" => {"Джерси", "JEY"},
      "441624" => {"Остров Мэн", "IMN"},
      "4428" => {"Северная Ирландия", "GBR"},
      "45" => {"Дания", "DNK"},
      "45" => {"Свободный город Христиания (Квартал Копенгагена, Дания)", "DNK"},
      "46" => {"Швеция", "SWE"},
      "47" => {"Норвегия", "NOR"},
      "47" => {"Шпицберген", "NOR"},
      "48" => {"Польша", "POL"},
      "49" => {"Германия", "DEU"},
      "500" => {"Фолклендские острова", "FLK"},
      "501" => {"Белиз", "BLZ"},
      "502" => {"Гватемала", "GTM"},
      "503" => {"Сальвадор", "SLV"},
      "504" => {"Гондурас", "HND"},
      "505" => {"Никарагуа", "NIC"},
      "506" => {"Коста-Рика", "CRI"},
      "507" => {"Панама", "PAN"},
      "508" => {"Сен-Пьер и Микелон", "FRA"},
      "509" => {"Гаити", "HTI"},
      "51" => {"Перу", "PER"},
      "52" => {"Мексика", "MEX"},
      "53" => {"Куба", "CUB"},
      "54" => {"Аргентина", "ARG"},
      "55" => {"Бразилия", "BRA"},
      "56" => {"Чили", "CHL"},
      "57" => {"Колумбия", "COL"},
      "58" => {"Венесуэла", "VEN"},
      "590" => {"Гваделупа", "GLP"},
      "590" => {"Сен-Бартельми", "GLP"},
      "590" => {"Сен-Мартен (Франция)", "GLP"},
      "591" => {"Боливия", "BOL"},
      "592" => {"Гайана", "GUY"},
      "593" => {"Эквадор", "ECU"},
      "594" => {"Фр. Гвиана", "GUF"},
      "595" => {"Парагвай", "PRY"},
      "596" => {"Мартиника", "MTQ"},
      "597" => {"Суринам", "SUR"},
      "598" => {"Уругвай", "URY"},
      "599" => {"Карибского бассейна Нидерланды", ""},
      "599" => {"Кюрасао", "CUW"},
      "60" => {"Малайзия", "MYS"},
      "61" => {"Австралия", "AUS"},
      "6189162" => {"Кокосовые острова", "CCK"},
      "6189164" => {"Остров Рождества", "CXR"},
      "62" => {"Индонезия", "IDN"},
      "63" => {"Филиппины", "PHL"},
      "64" => {"Новая Зеландия", "NZL"},
      "65" => {"Сингапур", "SGP"},
      "66" => {"Таиланд", "THA"},
      "670" => {"Восточный Тимор", "TLS"},
      "6721" => {"Австралийская антарктическая база", "ATA"},
      "6723" => {"Норфолк (остров)", "NFK"},
      "673" => {"Бруней-Даруссалам", "BRN"},
      "674" => {"Науру", "NRU"},
      "675" => {"Папуа-Новая Гвинея", "PNG"},
      "676" => {"Тонга", "TON"},
      "677" => {"Соломоновы Острова", "SLB"},
      "678" => {"Вануату", "VUT"},
      "679" => {"Фиджи", "FJI"},
      "680" => {"Палау", "PLW"},
      "681" => {"Уоллис и Футуна", "WLF"},
      "682" => {"Острова Кука", "COK"},
      "683" => {"Ниуэ", "NIU"},
      "685" => {"Самоа", "WSM"},
      "686" => {"Кирибати", "KIR"},
      "687" => {"Новая Каледония", "NCL"},
      "688" => {"Тувалу", "TUV"},
      "689" => {"Французская Полинезия (Таити)", "PYF"},
      "690" => {"Токелау", "TKL"},
      "691" => {"Ф.Ш. Микронезии", "FSM"},
      "692" => {"Маршалловы Острова", "MHL"},
      "7" => {"Россия", "RUS"},
      "7" => {"Казахстан", "KAZ"},
      "7840" => {"Абхазия", "GEO"},
      "800" => {"Международная бесплатная связь", ""},
      "808" => {"Международного разделения расходов", ""},
      "81" => {"Япония", "JPN"},
      "82" => {"Южная Корея", "KOR"},
      "84" => {"Вьетнам", "VNM"},
      "850" => {"Северная Корея", "PRK"},
      "852" => {"Гонконг", "HKG"},
      "853" => {"Макао", "MAC"},
      "855" => {"Камбоджа", "KHM"},
      "856" => {"Лаос", "LAO"},
      "86" => {"Китай", "CHN"},
      "86" => {"Тибет", "CHN"},
      "86" => {"Внутренняя Монголия", "CHN"},
      "875" => {"морской подвижной службы", ""},
      "876" => {"морской подвижной службы", ""},
      "877" => {"морской подвижной службы", ""},
      "878" => {"универсального личного номера", ""},
      "879" => {"национальных некоммерческих целей", ""},
      "880" => {"Бангладеш", "BGD"},
      "881" => {"услуги мобильной спутниковой связи", ""},
      "8811" => {"Услуги мобильной спутниковой связи", ""},
      "8812" => {"Услуги мобильной спутниковой связи", ""},
      "8813" => {"Услуги мобильной спутниковой связи", ""},
      "8816" => {"Иридиум (спутниковая связь)", ""},
      "8817" => {"Иридиум (спутниковая связь)", ""},
      "8818" => {"Глобалстар (спутниковая связь)", ""},
      "8819" => {"Глобалстар (спутниковая связь)", ""},
      "882" => {"Код международных сетей", ""},
      "88210" => {"BT (спутниковая связь)", ""},
      "88212" => {"WorldCom (спутниковая связь)", ""},
      "88213" => {"Telespazio (спутниковая связь)", ""},
      "88215" => {"REACH (спутниковая связь)", ""},
      "88216" => {"Thuraya (спутниковая связь)", ""},
      "88220" => {"ACeS (спутниковая связь)", ""},
      "88222" => {"Cable & Wireless (спутниковая связь)", ""},
      "88223" => {"Sita-Equant (спутниковая связь)", ""},
      "88224" => {"TeliaSonera (спутниковая связь)", ""},
      "88230" => {"Singapore Telecom (спутниковая связь)", ""},
      "88231" => {"Telekom Malaysia (спутниковая связь)", ""},
      "88232" => {"Maritime Communications Partners (спутниковая связь)", ""},
      "88233" => {"Oration Technologies (спутниковая связь)", ""},
      "88234" => {"Global Networks Switzerland (спутниковая связь)", ""},
      "88235" => {"Jasper Wireless (спутниковая связь)", ""},
      "88236" => {"Jersey Telecom (спутниковая связь)", ""},
      "88237" => {"Cingular Wireless (спутниковая связь)", ""},
      "88239" => {"Vodaphone Malta (спутниковая связь)", ""},
      "88240" => {"Oy Cubio (спутниковая связь)", ""},
      "88241" => {"Intermatica (спутниковая связь)", ""},
      "88242" => {"Seanet Maritime (спутниковая связь)", ""},
      "88243" => {"Ukr. Radiosys. Beeline (спутниковая связь)", ""},
      "88245" => {"Telecom Italia (спутниковая связь)", ""},
      "88298" => {"OnAir N.V. (спутниковая связь)", ""},
      "88299" => {"Telenor AeroMobile (спутниковая связь)", ""},
      "883" => {"Код международных сетей", ""},
      "883120" => {"Международные сети", ""},
      "8835100" => {"Voxbone (спутниковая связь)", ""},
      "8835100" => {"Bandwidth (спутниковая связь)", ""},
      "8835130" => {"Sipme (спутниковая связь)", ""},
      "886" => {"Тайвань", "TWN"},
      "888" => {"телекоммуникации для ликвидации последствий бедствий", ""},
      "90" => {"Турция", "TUR"},
      "90392" => {"Северный Кипр", "TUR"},
      "91" => {"Индия", "IND"},
      "92" => {"Пакистан", "PAK"},
      "93" => {"Афганистан", "AFG"},
      "94" => {"Шри-Ланка", "LKA"},
      "95" => {"Бирма (Мьянма)", "MMR"},
      "960" => {"Мальдивские острова", "MDV"},
      "961" => {"Ливан", "LBN"},
      "962" => {"Иордания", "JOR"},
      "963" => {"Сирия", "SYR"},
      "964" => {"Ирак", "IRQ"},
      "965" => {"Кувейт", "KWT"},
      "966" => {"Саудовская Аравия", "SAU"},
      "967" => {"Йемен", "YEM"},
      "968" => {"Оман", "OMN"},
      "970" => {"Палестина", "PSE"},
      "971" => {"Объединенные Арабские Эмираты", "ARE"},
      "972" => {"Израиль", "ISR"},
      "973" => {"Бахрейн", "BHR"},
      "974" => {"Катар", "QAT"},
      "975" => {"Бутан", "BTN"},
      "976" => {"Монголия", "MNG"},
      "977" => {"Непал", "NPL"},
      "979" => {"международных услуг премиум-курс", ""},
      "98" => {"Иран", "IRN"},
      "992" => {"Таджикистан", "TJK"},
      "993" => {"Туркменистан", "TKM"},
      "994" => {"Азербайджан", "AZE"},
      "995" => {"Грузия", "GEO"},
      "996" => {"Киргизия", "KGZ"},
      "9971" => {"Южная Осетия", "GEO"},
      "998" => {"Узбекистан", "UZB"},
      "999" => {"будущих глобальных услуг", ""}
    }
  end
  defp geo_number(to_number) do
     geo_number(to_number, -11,
      %{
        nil => ["HZ","HZ"],
        "1" => ["Замбия","ZAM"],
        "2" => ["Африка", "AFR"],
        "3" => ["Америка", "AME"],
        "4" => ["Бразилия","BRA"],
        "5" => ["Аргентина","ARG"],
        "6" => ["Ямайка","YAM"],
        "7" => %{
          :default => ["Россия","RUS"],
          "7" => %{
            :default => ["Казахстан","KZ"],
            "0" => %{
              :default => ["Казахстан","KZ"],
              "5" => ["Казахстан сотовый","KZ-Kcell"]
            },
            "2" => %{
              :default => ["Казахстан","KZ"],
              "7" => ["Казахстан Алматы городской","KZ-almaty"]
            }
          }
        },
        "8" => ["Германия","GER"],
        "9" => ["Барселона","BAR"],
        "0" => ["Франция","FRA"]
      }
    )
  end
  defp geo_number(to_number,index,spr) do
    IO.puts "hello world"
  end
  
  def listen(port) do
    IO.puts "Launching server on localhost on port #{port}"
    #0a8a5add1168f86012584760697d66a3@31.171.171.10:5060
    #[
    #  [{102, :invite}, "invite",         "2018-11-21 14:15:17.622647Z", 1],
    #  [{102, :invite}, "Unauthorized",   "2018-11-21 14:15:17.628896Z", -1],
    #  [{102, :ack}, "ack",               "2018-11-21 14:15:17.630057Z", -1],
    #  [{103, :invite}, "invite",         "2018-11-21 14:15:17.631793Z", -1],
    #  [{103, :invite}, "Trying",         "2018-11-21 14:15:17.636124Z", -1],
    #  [{103, :invite}, "Ringing",        "2018-11-21 14:15:18.093422Z", -1],
    #  [{103, :invite}, "OK",             "2018-11-21 14:15:25.462362Z", -1],
    #  [{103, :ack}, "ack",               "2018-11-21 14:15:25.464033Z", -1],
    #  [{104, :bye}, "bye",               "2018-11-21 14:16:29.595013Z", -1],
    #  [{104, :bye}, "OK",                "2018-11-21 14:16:29.597719Z", -1]
    #]
    #0f85efe52e9b2437737dab523d930b17@31.171.171.10:5060
    #[
    #  [{102, :invite}, "invite",           "2018-11-21 15:19:22.756085Z", 1],
    #  [{102, :invite}, "Trying",           "2018-11-21 15:19:22.769004Z", 2],
    #  [{102, :invite}, "Session Progress", "2018-11-21 15:19:22.991478Z", 3],
    #  [{102, :invite}, "OK",               "2018-11-21 15:19:37.875661Z", 4],
    #  [{102, :ack}, "ack",                 "2018-11-21 15:19:37.877130Z", 5],
    #  [{103, :invite}, "invite",           "2018-11-21 15:19:37.880034Z", -1],
    #  [{103, :invite}, "Trying",           "2018-11-21 15:19:37.882820Z", -1],
    #  [{103, :invite}, "OK",               "2018-11-21 15:19:37.884540Z", -1],
    #  [{103, :ack}, "ack",                 "2018-11-21 15:19:37.885811Z", -1],
    #  [{102, :bye}, "bye",                 "2018-11-21 15:19:44.577077Z", -1],
    #  [{102, :bye}, "OK",                  "2018-11-21 15:19:44.579132Z", -1]
    #]
    #520552851-8066-220@BJC.BGI.HC.DA
    #[
    #  [{2190, :invite}, "invite",  "2018-11-21 14:57:54.109421Z", 1],
    #  [{2190, :ack}, "ack",        "2018-11-21 14:57:54.113771Z", -1],
    #  [{2191, :invite}, "invite",  "2018-11-21 14:57:54.118401Z", -1],
    #  [{2191, :ack}, "ack",        "2018-11-21 14:57:55.103104Z", -1],
    #  [{2192, :bye}, "bye",        "2018-11-21 14:59:26.113794Z", -1]
    #]
    server = Socket.UDP.open!(port)
    serve( server, 1, %{} )
  end

  def serve( server, count, mydata ) do
  
    {data, client} = server |> Socket.Datagram.recv!
    mydata = 
      case Sippet.Message.parse ( data ) do
        {:error, :missing_method} -> 
          mydata
        { :ok, data1 } -> 
          call_id = data1.headers.call_id
          {from_empty, from, from_tag} = data1.headers.from
          {to_empty, to, to_tag} = data1.headers.to
          {cseq_num, cseq_type} = data1.headers.cseq
          
          #IO.inspect from #IO.inspect to #IO.puts to_string(call_id) <> " - " <> to_string(cseq_num) <> " - " <> to_string(cseq_type)

          { start_line, status_code } = if is_nil(Map.get(data1.start_line, :reason_phrase)) do { data1.start_line.method, 0 } else {data1.start_line.reason_phrase, data1.start_line.status_code} end
          new_record = [[data1.headers.cseq] ++ [{to_string(start_line), status_code}] ++ [to_string(DateTime.utc_now()) ]]
          call_id_list = Map.get(mydata,call_id);
          call_id_list = if is_nil(call_id_list) do new_record else call_id_list ++ new_record end
          
          if !(cseq_type == :register or cseq_type == :options) do
            #IO.inspect  call_id_list
            data_after_analyze = analyze(call_id_list)
            IO.puts call_id
            IO.inspect data_after_analyze
            #IO.inspect call_id_list
          end
          #if to_string(start_line) != to_string("register") do
          #  IO.puts to_string(start_line) <> ", Call-id: " <> to_string(call_id) <> ", From, " <> to_string(from.authority) <> ", To, " <> to_string(to.authority) <> ", " <> to_string(DateTime.utc_now())
          #end
          if (cseq_type == :register or cseq_type == :options) do mydata else Map.put(mydata, call_id, call_id_list) end
      end
    #if rem(count,25) == 0 do IO.inspect mydata end
    serve( server, count + 1, mydata )
  end
  
end

"""
%Sippet.Message{
  body: "v=0\r\no=520 16264 18299 IN IP4 192.168.1.83\r\ns=call\r\nc=IN IP4 192.168.1.83\r\nt=0 0\r\nm=audio 25282 RTP/AVP 0 101\r\na=rtpmap:0 pcmu/8000\r\na=rtpmap:8 pcma/8000\r\na=rtpmap:101 telephone-event/8000\r\na=fmtp:101 0-11\r\n",
  headers: %{
    allow: ["ACK", "BYE", "CANCEL", "INFO", "INVITE", "MESSAGE", "NOTIFY",
     "OPTIONS", "PRACK", "REFER", "REGISTER", "SUBSCRIBE", "UPDATE", "PUBLISH"],
    call_id: "260775841-205835178-247388197",
    contact: [
      {"",
       %Sippet.URI{
         authority: "520@163.172.99.80:52230",
         headers: nil,
         host: "163.172.99.80",
         parameters: nil,
         port: 52230,
         scheme: "sip",
         userinfo: "520"
       }, %{}}
    ],
    content_length: 207,
    content_type: {{"application", "sdp"}, %{}},
    cseq: {1, :invite},
    from: {"",
     %Sippet.URI{
       authority: "520@31.171.171.3",
       headers: nil,
       host: "31.171.171.3",
       parameters: nil,
       port: 5060,
       scheme: "sip",
       userinfo: "520"
     }, %{"tag" => "529482116"}},
    max_forwards: 70,
    to: {"",
     %Sippet.URI{
       authority: "23360048224815895@31.171.171.3",
       headers: nil,
       host: "31.171.171.3",
       parameters: nil,
       port: 5060,
       scheme: "sip",
       userinfo: "23360048224815895"
     }, %{}},
    user_agent: "fdgddfg546df4g8d5f",
    via: [
      {{2, 0}, :udp, {"163.172.99.80", 52230},
       %{"branch" => "z9hG4bK731553692"}}
    ]
  },
  start_line: %Sippet.Message.RequestLine{
    method: :invite,
    request_uri: %Sippet.URI{
      authority: "23360048224815895@31.171.171.3",
      headers: nil,
      host: "31.171.171.3",
      parameters: nil,
      port: 5060,
      scheme: "sip",
      userinfo: "23360048224815895"
    },
    version: {2, 0}
  },
  target: nil
}
"""
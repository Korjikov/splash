defmodule Chat.RoomChannel do
  use Phoenix.Channel
  require Logger

  @doc """
  Authorize socket to subscribe and broadcast events on this channel & topic

  Possible Return Values

  `{:ok, socket}` to authorize subscription for channel for requested topic

  `:ignore` to deny subscription/broadcast on this channel
  for the requested topic
  """
  def join("rooms:lobby", message, socket) do
    Process.flag(:trap_exit, true)
    #:timer.send_interval(5000, :ping)
    :timer.send_interval(1000, :ping1)
    send(self, {:after_join, message})

    {:ok, socket}
  end

  def join("rooms:" <> _private_subtopic, _message, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_info({:after_join, msg}, socket) do
    broadcast! socket, "user:entered", %{user: msg["user"]}
    push socket, "join", %{status: "connected"}
    {:noreply, socket}
  end
  def handle_info(:ping, socket) do
    push socket, "new1:msg1", %{user: "SYSTEM", body: "pingping"}
    {:noreply, socket}
  end
  
  
  def handle_info(:ping1, socket) do
    {:ok, conn} = Redix.start_link(host: "splash_redis_1", port: 6379)
    {:ok, size } = Redix.command(conn, ["llen", "list:alert"])
    {:ok, data_all } = Redix.command(conn, ["lrange", "list:alert", "0", "24"])
    #IO.inspect data_all
    
    #Enum.map([1, 2, 3], fn x -> x * 2 end)
    
    message = Enum.map(data_all, fn data -> 
        data = to_string(data)
        if (String.length data) > 0 do 
          [d_num, d_numa, d_numb, d_datetime] = String.split( data, ":" )
          numbs = String.split( d_numb, "," )
          numbs = List.delete_at(numbs, 0)
          numbs = List.delete_at(numbs, length(numbs)-1)
          {intVal, ""} = :string.to_integer(d_datetime)
          {:ok, datetime} = DateTime.from_unix(intVal)
          push socket, "new:msg", %{numa: to_string(d_numa), numb: Enum.join(numbs, ":"), date: to_string(datetime), length1: length(numbs), size: size, a1: %{b1: "werwe"} }
          %{numa: to_string(d_numa), numb: Enum.join(numbs, ":"), date: to_string(datetime), length1: length(numbs), size: size }
        else
          %{}
        end
      end)
    #IO.inspect List.to_tuple(message)
    #IO.puts to_string()
    #push socket, "new:msg", List.to_tuple(message)
    {:noreply, socket}
  end

  def terminate(reason, _socket) do
    Logger.debug"> leave #{inspect reason}"
    :ok
  end

  def handle_in("new:msg", msg, socket) do
    broadcast! socket, "new:msg", %{user: msg["user"], body: msg["body"]}
    {:reply, {:ok, %{msg: msg["body"]}}, assign(socket, :user, msg["user"])}
  end
end

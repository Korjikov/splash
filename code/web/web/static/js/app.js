import {Socket, LongPoller} from "phoenix"

class App {

  static init(){
    let socket = new Socket("/socket", {
      logger: ((kind, msg, data) => { console.log(`${kind}: ${msg}`, data) })
    })

    socket.connect({user_id: "123"})
    var $status    = $("#status")
    var $messages  = $("#messages")
    var $input     = $("#message-input")
    var $username  = $("#username")
    var $button1   = $("#button-input")

    socket.onOpen( ev => console.log("OPEN", ev) )
    socket.onError( ev => console.log("ERROR", ev) )
    socket.onClose( e => console.log("CLOSE", e))

    var chan = socket.channel("rooms:lobby", {})
    chan.join().receive("ignore", () => console.log("auth error"))
               .receive("ok", () => console.log("join ok"))
               .after(10000, () => console.log("Connection interruption"))
    chan.onError(e => console.log("something went wrong", e))
    chan.onClose(e => console.log("channel closed", e))

    $button1.on("click", function(){
        chan.push("new:msg", {user: "sample", body: "value"})
    });
    
    $input.off("keypress").on("keypress", e => {
      if (e.keyCode == 13) {
        chan.push("new:msg", {user: $username.val(), body: $input.val()})
        $input.val("")
      }
    })

    chan.on("new:msg", msg => {
      $("#count_incendent").html(msg.size);
      $("#count_incendent1").html(msg.size);
      $messages.append(this.messageTemplate(msg))
      scrollTo(0, document.body.scrollHeight)
    })
    
    chan.on("new1:msg1", msg => {
      $messages.append(this.messageTemplate1(msg))
      scrollTo(0, document.body.scrollHeight)
    })

    chan.on("user:entered", msg => {
      var username = this.sanitize(msg.user || "anonymous")
      // $messages.append(`<br/><i>[${username} entered]</i>`)
    })
  }

  static sanitize(html){ return $("<div/>").text(html).html() }

  static messageTemplate(msg){
    //let username = this.sanitize(msg.user || "anonymous")
    //let body     = this.sanitize(msg.body)
    var myarr = msg.numb.split(":");
    
    var string1 = "";
    myarr.forEach(function(item, i, arr) {
      string1 = string1 + item + "<br>";
    });
    return(`<div class="row" style="border-bottom: 1px solid #DDDDDD;">
      <div class="col-sm-2" style="display:flex;justify-content:center;align-items:center;text-align:center;">${msg.date}</div>
      <div class="col-sm-1" style="display:flex;justify-content:center;align-items:center;text-align:center;"><span class="label label-danger">красный</span></div>
      <div class="col-sm-2" style="display:flex;justify-content:center;align-items:center;text-align:center;">${msg.numa}</div>
      <div class="col-sm-1" style="display:flex;justify-content:center;align-items:center;text-align:center;"><span class="label label-success">дубль</span></div>
      <div class="col-sm-2" style="justify-content:center;align-items:center;text-align:center;">${string1}</div>
      <div class="col-sm-1" style="display:flex;justify-content:center;align-items:center;text-align:center;">${msg.length1}</div>
      <div class="col-sm-2" style="display:flex;justify-content:center;align-items:center;text-align:center;">
        <button type="button" class="btn btn-primary btn-xs">Email</button>
        <button type="button" class="btn btn-primary btn-xs">API</button>
      </div>
      <div class="col-sm-1" style="display:flex;justify-content:center;align-items:center;text-align:center;"><button type="button" class="btn btn-primary btn-xs">Закрыть</button></div>
      </div>`)
    // return(`<p><a href='#'>[${username}]</a>&nbsp; ${body}</p>`)
  }
  
  static messageTemplate1(msg){
    let body     = this.sanitize(msg.body)
    return(`<div class="row">
      <div class="col-sm-12">${body}</div>
      </div>`)
  }

}

$( () => App.init() )

export default App


local cdr_line = redis.call('LPOP', 'cdrlist');

if not cdr_line then 
	return 0
end

local cdr = cjson.decode(cdr_line)

local exist = redis.call( 'SADD', cdr.src, cdr.dst .. ":" .. cdr.times .. ":" .. cdr.timee );
if not exist then 
	return {"not exist"}
end

local temp1 = redis.call('sscan', cdr.src, '0', 'COUNT', '999');
local ress = -1
--local debug = cdr.times .. "-" .. cdr.timee .. "  "
if #temp1[2] > 1 then
	cdr.times = tonumber(cdr.times)
	cdr.timee = tonumber(cdr.timee)
	for k,v in pairs(temp1[2]) do
		local dst,ts,te = string.match( v, "(%w+):(%w+):(%w+)")
		ts = tonumber(ts)
		te = tonumber(te)
		if 	( cdr.times >= ts and cdr.times <= te ) or 
			( cdr.timee >= ts and cdr.timee <= te ) or
			( cdr.times <= ts and cdr.timee >= te )
			then 
			ress = ress + 1
		end
	end
	return ress
end

return ress

